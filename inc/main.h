/**
 * @file   main.h
 *
 * @Author Koremblum, Nicolás Mariano (marianotec7@gmail.com)
 *
 * @date   6/9/2015 [DD/MM/YYYY]
 *
 * @brief  Brief description of file.
 */

#ifndef INC_MAIN_H_
#define INC_MAIN_H_

/* **************************************************************** */
/* 								INCLUDEs							*/

#include	<stdio.h>

#include	<stdlib.h>

#include 	"bbb.h"

/* **************************************************************** */
/* 								DEFINEs								*/

// CONFIGURATION DEFINEs

#define		MAX_BUFFER_SIZE			100

// GENERIC DEFINEs

#define		EXIT_VALUE				-99

// FUNCTIONAL DEFINEs (DO NOT CHANGE)

// OTHER DEFINEs

/* **************************************************************** */
/* 								IFDEFs								*/

//	Add ifdef's here

/* **************************************************************** */
/* 								MACROs								*/

#define		KEYBOARD_GET_OPTION(x)	fgets(x,MAX_BUFFER_SIZE,stdin);x[1]='\0'

#define		KEYBOARD_GET_VALUE(x)	KEYBOARD_GET_OPTION(x)

#define		KEYBOARD_GET_DIR(x)		KEYBOARD_GET_OPTION(x)

#define		KEYBOARD_GET_GPIO(x)	fgets(x,MAX_BUFFER_SIZE,stdin);x[4]='\0'

#define		IS_EXIT(x)				( (x[0] == 'n') || (x[0] == 'N') )

#define		IS_ERROR(x)				( ( x < 0 ) ? 1:0 )

#define		CLEAR_SCREEN			system("clear");

/* **************************************************************** */
/* 						     EXTERNAL GLOBALs						*/

//	Add global variable's here

/* **************************************************************** */
/* 						     	PROTOTYPEs							*/

int GPIOfunction(void);

#endif /* INC_MAIN_H_ */
