mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
PROJECT := $(notdir $(patsubst %/,%,$(dir $(mkfile_path))))

SRC_PATH := ./src
BBB_PATH := ./BBB
INC_PATH := ./inc
OUT_PATH := .
OBJ_PATH := .

INCLUDES := -I./inc -I./BBB
CFLAGS   := -Wall -c

SRC_FILES := $(wildcard $(SRC_PATH)/*.c)
BBB_FILES := $(wildcard $(BBB_PATH)/*.c)
ASM_FILES := $(wildcard $(SRC_PATH)/*.S)
INC_FILES := $(wildcard $(INC_PATH)/*.h)
OBJ_FILES := $(subst $(SRC_PATH),$(OBJ_PATH),$(SRC_FILES:.c=.o))
OBJ_FILES += $(subst $(SRC_PATH),$(OBJ_PATH),$(ASM_FILES:.S=.o))
OBJ_FILES += $(subst $(BBB_PATH),$(OBJ_PATH),$(BBB_FILES:.c=.o))

$(PROJECT): $(OBJ_FILES)
	@echo "*** Linking project $(PROJECT) ***"
	gcc $(LIB_PATH) $(LFLAGS) $(LD_FILE) -o $(OUT_PATH)/$(PROJECT).axf $(OBJ_FILES) $(LIBS)
	@echo ""

$(OBJ_PATH)/%.o: $(BBB_PATH)/%.c
	@echo "*** Compiling C file $< ***"
	gcc $(INCLUDES) $(CFLAGS) $< -o $@ 
	@echo ""

$(OBJ_PATH)/%.o: $(SRC_PATH)/%.c
	@echo "*** Compiling C file $< ***"
	gcc $(INCLUDES) $(CFLAGS) $< -o $@ 
	@echo ""

$(OBJ_PATH)/%.o: $(SRC_PATH)/%.S
	@echo "*** Compiling Assembly file $< ***"
	gcc $(INCLUDES) $(CFLAGS) $< -o $@
	@echo ""

clean:
	rm -f $(OBJ_PATH)/*.*
	rm -f $(OUT_PATH)/*.*
