/**
 * DOXYGEN COMMENTS
 *
 * @file   main.c
 *
 * @Author Koremblum, Nicolás Mariano (marianotec7@gmail.com)
 *
 * @date   6/9/2015 [DD/MM/YYYY]
 *
 * @brief  Programa ejemplo de como utilizar algunos periféricos de la BeagleBoneBlack
 *
 * ! Este ejemplo es válido, en principio, solo en la distribución Angström.
 *
 * Periféricos soportados:
 * 							-GPIO
 *
 * @todo   Agregar funcionalidad de otros periféricos
 */

/* **************************************************************** */
/* 								INCLUDEs							*/

#include "main.h"

/* **************************************************************** */
/* 								DEFINEs								*/

// Add private define's here

/* **************************************************************** */
/* 								MACROs								*/

// Add private macro's here

/* **************************************************************** */
/* 								GLOBALs								*/

// Add global variable's here

/* **************************************************************** */
/* 								 CODE								*/

int main(void)
{
	int retval = -1;

	CLEAR_SCREEN;

	printf("Bienvenido al programa de prueba de la BeagleBoneBlack (BBB)");

	retval = GPIOfunction();

	return retval;
}

/** ****************************************************************************************************** **/

int GPIOfunction(void)
{
	int gpio = -1;

	int retval = -1;

	char opt[MAX_BUFFER_SIZE];

	printf("\n");
	printf("\nIngrese un GPIO(#): ");
	KEYBOARD_GET_GPIO(opt);

	gpio = atoi(opt);

	// La instrucción "bbb_exportGpioPin" habilita los recursos del GPIO en el sistema
	bbb_exportGpioPin(gpio);

	while( retval != EXIT_VALUE )
	{
		printf("\n1.-Salida \n2.-Entrada");
		printf("\nElija el modo de direccionamiento del GPIO: ");
		KEYBOARD_GET_DIR(opt);

		if( ( retval = atoi(opt) ) == 1 )
		{
			printf("\nSeteando el GPIO%d como salida...",gpio);

			// Seteo el direccionamient del GPIO como output
			if( IS_ERROR(bbb_setGpioPinDir(gpio,OUT)) )	perror("Error en setDir: ");
		}

		else
		{
			printf("\nSeteando el GPIO%d como entrada...",gpio);

			// Seteo el direccionamient del GPIO como input
			if( IS_ERROR(bbb_setGpioPinDir(gpio,IN)) )	perror("Error en setDir: ");
		}

		// Obtengo el direccionamiento (IN o OUT) del GPIO
		retval = bbb_getGpioPinDir(gpio);

		printf("\nDirección: %d",retval);

		if( IS_ERROR(retval) )	perror("Error en getDir: ");

		if( IN_DIR == retval )
		{
			retval = bbb_getGpioPinValue(gpio);

			if( IS_ERROR(retval) )	perror("Error en getPin: ");

			printf("\n");
			printf("\n->Pin value = %d",retval);
		}

		else
		{
			retval = bbb_getGpioPinValue(gpio);

			if( IS_ERROR(retval) )	perror("Error en getPin: ");

			printf("\n\n->Pin value = %d",retval);
			printf("\n\nAsigne un valor al GPIO (1 ó 0): ");
			KEYBOARD_GET_VALUE(opt);

			if( IS_ERROR(bbb_setGpioPinValue(gpio,(bool)atoi(opt))) )	perror("Error en setPin: ");

			printf("\n->Pin value = %d",bbb_getGpioPinValue(gpio));
		}

		printf("\nDesea continuar? Y/n: ");
		KEYBOARD_GET_OPTION(opt);

		if( IS_EXIT(opt) )
		{
			printf("\nSaliendo...");
			printf("\n->GPIO dir = %d",bbb_getGpioPinDir(gpio));
			printf("\n->Pin value = %d",bbb_getGpioPinValue(gpio));

			bbb_unexportGpioPin(gpio);

			retval = EXIT_VALUE;
		}
	}

	printf("\n\n");

	return 0;
}
