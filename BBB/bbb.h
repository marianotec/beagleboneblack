/**
 * @file   bbb.h
 *
 * @Author Koremblum, Nicolás Mariano (marianotec7@gmail.com)
 *
 * @date   5/9/2015 [DD/MM/YYYY]
 *
 * @brief  Brief description of file.
 */

#ifndef BBB_H_
#define BBB_H_

/* **************************************************************** */
/* 								INCLUDEs							*/

//	Add include's here

#include	<stdio.h>

#include	<string.h>

#include	<stdbool.h>

#include	<stdlib.h>

/* **************************************************************** */
/* 								DEFINEs								*/

// CONFIGURATION DEFINEs

#define		MAX_DIR_SIZE		100

#define		DEFAULT_GPIO_PATH	"/sys/class/gpio"

// GENERIC DEFINEs

#define		FILE_ERROR			-1

#define		SET_ERROR			-2

#define		IN_MODE_ERROR		-3	//Error al querer modificar el valor de un pin en modo entrada

// FUNCTIONAL DEFINEs (DO NOT CHANGE)

#define		IN					0

#define		OUT					1

#define		IN_DIR				0

#define		OUT_DIR				1

#define		SET_DIR_IN			IN

#define		SET_DIR_OUT			OUT

#define		SET_DIR_HIGH		SET_DIR_OUT

#define		SET_DIR_LOW			SET_DIR_IN

#define		VALUE_SIZE			5

#define 	WR					"r+"

// OTHER DEFINEs

/* **************************************************************** */
/* 								IFDEFs								*/

//	Add ifdef's here

/* **************************************************************** */
/* 								MACROs								*/

#define 	VAL2DIR(x)			( (IN == x) ? "in":"out" )

#define		DIR2VAL(x)			( ( strncmp(x,"in",2) == 0 ) ? IN:OUT )

/* **************************************************************** */
/* 						     EXTERNAL GLOBALs						*/

//	Add global variable's here

/* **************************************************************** */
/* 						     	PROTOTYPEs							*/

int bbb_getGpioPinValue(int GPIO);

int bbb_getGpioPinDir(int GPIO);

int bbb_setGpioPinDir(int GPIO, bool value);

int bbb_setGpioPinValue(int GPIO, bool value);

void bbb_unexportGpioPin(int GPIO);

void bbb_exportGpioPin(int GPIO);

#endif /* BBB_H_ */
