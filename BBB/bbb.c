/**
 * DOXYGEN COMMENTS
 *
 * @file   bbb.c
 *
 * @Author Koremblum, Nicolás Mariano (marianotec7@gmail.com)
 *
 * @date   5/9/2015 [DD/MM/YYYY]
 *
 * @brief  Brief description of file.
 *
 * Detailed description of file.
 *
 * @todo   What's to do?
 */

/* **************************************************************** */
/* 								INCLUDEs							*/

#include "bbb.h"

/* **************************************************************** */
/* 								DEFINEs								*/

// Add private define's here

/* **************************************************************** */
/* 								MACROs								*/

// Add private macro's here

/* **************************************************************** */
/* 								GLOBALs								*/

// Add global variable's here

/* **************************************************************** */
/* 								 CODE								*/

void bbb_exportGpioPin(int GPIO)
{
	char gpioDir[MAX_DIR_SIZE];

	if(bbb_getGpioPinValue(GPIO) == FILE_ERROR)
	{
		sprintf(gpioDir,"echo %d > %s/export",GPIO,DEFAULT_GPIO_PATH);

		system(gpioDir);
	}
}

void bbb_unexportGpioPin(int GPIO)
{
	char gpioDir[MAX_DIR_SIZE];

	if(bbb_getGpioPinValue(GPIO) == FILE_ERROR)
	{
		sprintf(gpioDir,"echo %d > %s/unexport",GPIO,DEFAULT_GPIO_PATH);

		system(gpioDir);
	}
}

int bbb_setGpioPinValue(int GPIO, bool value)
{
	FILE * file = NULL;

	char gpioDir[MAX_DIR_SIZE];

	if( bbb_getGpioPinDir(GPIO) == IN )	return IN_MODE_ERROR;

	sprintf(gpioDir,"%s/gpio%d/value",DEFAULT_GPIO_PATH,GPIO);

	file = fopen(gpioDir,"r+");

	if( NULL == file )	return FILE_ERROR;

	if( fprintf(file,"%d",(int)value) < 0 )
	{
		fclose(file);

		return SET_ERROR;
	}

	fclose(file);

	return 0;
}

int bbb_setGpioPinDir(int GPIO, bool value)
{
	FILE * file = NULL;

	char dirDir[MAX_DIR_SIZE];

	sprintf(dirDir,"%s/gpio%d/direction",DEFAULT_GPIO_PATH,GPIO);

	file = fopen(dirDir,"r+");

	if( NULL == file )	return FILE_ERROR;

	if( fprintf(file,"%s",VAL2DIR(value)) < 0 )
	{
		fclose(file);

		return SET_ERROR;
	}

	fclose(file);

	return 0;
}

//todo setpinmode? (activo alto/bajo)

int bbb_getGpioPinDir(int GPIO)
{
	FILE * file = NULL;

	char value[VALUE_SIZE];

	char gpioDir[MAX_DIR_SIZE];

	sprintf(gpioDir,"%s/gpio%d/direction",DEFAULT_GPIO_PATH,GPIO);

	file = fopen(gpioDir,WR);

	if( NULL == file )	return FILE_ERROR;

	if( NULL == fgets(value,VALUE_SIZE,file) )
	{
		fclose(file);

		return SET_ERROR;
	}

	fclose(file);

	return DIR2VAL(value);
}

int bbb_getGpioPinValue(int GPIO)
{
	char value[VALUE_SIZE];

	FILE * file = NULL;

	char gpioDir[MAX_DIR_SIZE];

	sprintf(gpioDir,"%s/gpio%d/value",DEFAULT_GPIO_PATH,GPIO);

	file = fopen(gpioDir,WR);

	if( NULL == file )	return FILE_ERROR;

	if( NULL == fgets(value,VALUE_SIZE,file) )
	{
		fclose(file);

		return SET_ERROR;
	}

	fclose(file);

	return atoi(value);
}
